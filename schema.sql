
CREATE DATABASE seckill CHARSET utf8 COLLATE utf8_general_ci;

USE seckill;

CREATE TABLE seckill(
    `seckill_id` bigint NOT NULL AUTO_INCREMENT,
    `name` varchar(120) NOT NULL,
    `number` int NOT NULL,
    `start_time` timestamp NOT NULL,
    `end_time` timestamp NOT NULL,
    `create_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
    PRIMARY KEY (seckill_id),
    KEY idx_start_time (start_time),
    KEY idx_end_time(end_time),
    key idx_create_time(create_time)
) ENGINE=InnoDB AUTO_INCREMENT=101 DEFAULT CHARSET=utf8;

INSERT INTO seckill(name, number, start_time, end_time) VALUES
('1000:iphone6', 100, '2015-11-01', '2015-11-02'),
('500:ipad2'   , 200, '2015-11-01', '2015-11-02'),
('300:MI4'     , 300, '2015-11-01', '2015-11-02'),
('200:MInote'  , 400, '2015-11-01', '2015-11-02');

CREATE TABLE success_killed(
    `seckill_id` bigint NOT NULL,
    `user_phone` bigint NOT NULL,
    `state` tinyint NOT NULL DEFAULT -1,
    `create_time` timestamp NOT NULL,
    PRIMARY KEY (seckill_id, user_phone),
    KEY idx_create_time(create_time)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
