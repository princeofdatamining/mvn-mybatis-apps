package com.png0.seckill.seckill.dao;

import com.png0.seckill.seckill.entity.Seckill;

import org.apache.ibatis.annotations.Param;

import java.util.Date;
import java.util.List;

public interface SeckillDao {

    int reduceNumber(@Param("seckillId") long seckillId, @Param("killTime") Date killTime);

    Seckill queryById(@Param("seckillId") long seckillId);

    List<Seckill> queryAll(@Param("offset") int offset, @Param("limit") int limit);

}