package com.png0.seckill.succkilled.dao;

import com.png0.seckill.succkilled.entity.SuccessKilled;
import com.png0.seckill.seckill.entity.Seckill;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.Date;
import java.util.List;

// 配置 spring 和 junit 整合， junit 启动时加载 springIOC 容器
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations={"classpath:spring-dao.xml"})
public class SuccessKilledDaoTest {

    // 注入 Dao 实现类依赖
    @Autowired
    private SuccessKilledDao successKilledDao;

    private long phone = 13312345678l;

    @Test
    public void testInsertSuccessKilled() throws Exception {
        int insertedCount = successKilledDao.insertSuccessKilled(101l, phone);
        System.out.println("insertedCount:" + insertedCount);
    }

    @Test
    public void testQueryByIdWithSeckill() throws Exception {
        SuccessKilled successKilled = successKilledDao.queryByIdWithSeckill(101l, phone);
        System.out.println(successKilled);
    }

}