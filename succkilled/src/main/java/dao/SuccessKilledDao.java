package com.png0.seckill.succkilled.dao;

import com.png0.seckill.succkilled.entity.SuccessKilled;

import org.apache.ibatis.annotations.Param;

import java.util.Date;
import java.util.List;

public interface SuccessKilledDao {

    int insertSuccessKilled(@Param("seckillId") long seckillId, @Param("userPhone") long userPhone);

    SuccessKilled queryByIdWithSeckill(@Param("seckillId") long seckillId, @Param("userPhone") long userPhone);

}