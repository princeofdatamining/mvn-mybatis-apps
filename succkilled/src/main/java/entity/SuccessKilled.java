package com.png0.seckill.succkilled.entity;

import com.png0.seckill.seckill.entity.Seckill;

import lombok.Data;

import java.util.Date;

@Data
public class SuccessKilled {

    private Long seckillId;

    private Long userPhone;

    private Integer state;

    private Date createTime;

    private Seckill seckill;

}